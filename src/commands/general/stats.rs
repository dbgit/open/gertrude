use crate::utils::is_utils::is_user_mention;
use crate::utils::user::format_avatar;
use serenity::framework::standard::{macros::command, Args, CommandResult};
use serenity::model::channel::Message;
use serenity::prelude::*;

#[command]
pub fn stats(ctx: &mut Context, msg: &Message, args: Args) -> CommandResult {
    let mut args = args.clone();
    match args.single::<String>() {
        Ok(arg) => println!("{}", is_user_mention(&arg)),
        Err(_) => println!("anc"),
    }

    let user = match msg.mentions.first() {
        Some(user) => user.to_owned(),
        _ => msg.author.to_owned(),
    };

    let msg = msg.channel_id.send_message(&ctx.http, |m| {
        m.embed(|e| {
            let avatar = format_avatar(user.id, &user.avatar);
            e.author(|a| a.name(&user.name).icon_url(avatar));
            e.fields(vec![(
                "Discordian Since",
                user.created_at().format("%F").to_string(),
                true,
            )]);
            e
        });
        m
    });

    if let Err(why) = msg {
        println!("Error sending message: {:?}", why);
    }

    Ok(())
}
