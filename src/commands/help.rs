use serenity::prelude::Context;
use serenity::framework::standard::macros::help;
use serenity::model::prelude::{Message, UserId};
use serenity::framework::standard::{Args, HelpOptions, CommandGroup, CommandResult, help_commands};
use std::collections::HashSet;

#[help]
pub fn help_command(
    context: &mut Context,
    msg: &Message,
    args: Args,
    help_options: &'static HelpOptions,
    groups: &[&'static CommandGroup],
    owners: HashSet<UserId>,
) -> CommandResult {
    help_commands::with_embeds(context, msg, args, help_options, groups, owners)
}
