pub mod help;

mod general;

use general::ping::*;
use general::stats::*;
use serenity::framework::standard::macros::group;

group!({
    name: "general",
    options: {},
    commands: [ping, stats],
});
