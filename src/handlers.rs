use log::{debug, info};
use serenity::model::gateway::Activity;
use serenity::model::prelude::*;
use serenity::prelude::{Context, EventHandler};

pub struct Handler;

impl EventHandler for Handler {
    fn message(&self, _ctx: Context, msg: Message) {
        debug!("{:?} - {:?}", msg.author.name, msg.content);
    }

    fn ready(&self, ctx: Context, bot: Ready) {
        ctx.set_activity(Activity::playing("a generic dungeon crawler"));
        info!(
            "=== {} is ready, listening on {} servers ===",
            bot.user.name,
            bot.guilds.len()
        );
    }
}
