mod commands;
//mod database;
mod handlers;
mod utils;

use serenity::client::Client;
use serenity::framework::standard::StandardFramework;

use commands::*;
use commands::help::*;
use handlers::Handler;
use log::LevelFilter;
use utils::parameter_reader;

fn main() {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .filter_module("rust_discord_fun", LevelFilter::Trace)
        .parse_write_style("always")
        .init();
    let parameters = parameter_reader::load_from_env();

    let mut client = Client::new(parameters.discord_token, Handler).expect("Error creating client");

    client.with_framework(
        StandardFramework::new()
            .configure(|c| {
                c.prefix("$")
                    .ignore_bots(true)
                    .ignore_webhooks(true)
                    .allow_dm(false)
                    .case_insensitivity(true)
            })
            .help(&HELP_COMMAND)
            //.on_dispatch_error(error_handling::handle_command_error)
            .group(&GENERAL_GROUP),
    );

    if let Err(error) = client.start_autosharded() {
        println!("An error occurred while running the client: {:?}", error);
    }
}
