use serenity::framework::standard::DispatchError;
use serenity::model::prelude::Message;
use serenity::prelude::Context;

pub fn handle_command_error(context: &mut Context, msg: &Message, error: DispatchError) {
    let error_message = msg
        .channel_id
        .send_message(context, |x| x.content("something went wrong..."))
        .unwrap();
    println!("LOL NOPE!");
}
