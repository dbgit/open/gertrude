pub fn is_user_mention(c: &str) -> bool {
    c.starts_with("<@") && c.ends_with('>')
}
