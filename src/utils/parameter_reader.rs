use dotenv::dotenv;
use std::env;

pub struct Parameters {
    pub discord_token: String,
    pub database: DatabaseParameters,
}

pub struct DatabaseParameters {
    pub host: String,
    pub port: String,
    pub database: String,
    pub username: String,
    pub password: String,
}

impl ToString for DatabaseParameters {
    fn to_string(&self) -> String {
        format!(
            "postgres://{}:{}@{}:{}/{}",
            self.username, self.password, self.host, self.port, self.database
        )
    }
}


pub fn load_from_env() -> Parameters {
    dotenv().ok();

    Parameters {
        discord_token: get_var("DISCORD_TOKEN", "No Discord Token found"),
        database: DatabaseParameters {
            database: get_var_with_default("DB_DATABASE", "gertrude".to_owned()),
            host: get_var_with_default("DB_HOST", "127.0.0.1".to_owned()),
            port: get_var_with_default("DB_PORT", "5432".to_owned()),
            username: get_var_with_default("DB_USERNAME", "gertrude".to_owned()),
            password: get_var_with_default("DB_USERNAME", "".to_owned()),
        }
    }
}

fn get_var_with_default(env_var: &str, def: String) -> String {
    env::var(env_var).unwrap_or(def)
}

fn get_var(env_var: &str, error_message: &str) -> String {
    let val = env::var(env_var).expect(error_message);
    if val == "" { panic!("{}", error_message); }
    val
}
