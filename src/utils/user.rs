//use serenity::model::channel::*;
use serenity::model::id::UserId;

pub fn format_avatar(user_id: UserId, avatar: &Option<String>) -> String {
    match avatar {
        Some(avatar_id) => format!(
            "https://cdn.discordapp.com/avatars/{}/{}.png?size=128",
            user_id, avatar_id
        ),
        None => "https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png".to_owned(),
    }
}

//pub fn compare_mention_arg(mention: User, arg: &str) -> bool {}
